﻿using Blog.Domain.Core.Models;

namespace Blog.Infrastructure.Data.EntityFramework.Configurations
{
    class ArticleConfiguration : RecordConfiguration<Article>
    {
        public ArticleConfiguration()
        {
            Property(a => a.Name).IsRequired();
            Property(a => a.Text).HasMaxLength(200);
        }
    }
}
