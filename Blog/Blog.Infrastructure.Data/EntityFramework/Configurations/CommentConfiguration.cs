﻿using Blog.Domain.Core.Models;

namespace Blog.Infrastructure.Data.EntityFramework.Configurations
{
    class CommentConfiguration : RecordConfiguration<Comment>
    {
        public CommentConfiguration()
        {
            Property(c => c.Author).IsRequired();
        }
    }
}
