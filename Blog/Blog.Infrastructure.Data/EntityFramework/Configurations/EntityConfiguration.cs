﻿using Blog.Domain.Core.AbstractModels;
using System.Data.Entity.ModelConfiguration;

namespace Blog.Infrastructure.Data.EntityFramework.Configurations
{
    abstract class EntityConfiguration<T> : EntityTypeConfiguration<T>
        where T : Entity
    {
        protected EntityConfiguration()
        {
            HasKey(e => e.Id);
        }
    }
}
