﻿using Blog.Domain.Core.AbstractModels;

namespace Blog.Infrastructure.Data.EntityFramework.Configurations
{
    abstract class RecordConfiguration<T> : EntityConfiguration<T>
        where T : Record
    {
        protected RecordConfiguration()
        {
            Property(r => r.PublicationDate).IsRequired();
            Property(r => r.Text).IsRequired();
            Property(r => r.PublicationDate).HasColumnType("datetime2");
        }
    }
}
