﻿using Blog.Domain.Core.Models;
using Blog.Domain.Core.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Blog.Infrastructure.Data.EntityFramework
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<FullArticle> FullArticles { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new Configurations.ArticleConfiguration());
            modelBuilder.Configurations.Add(new Configurations.CommentConfiguration());

            modelBuilder.Entity<Article>()
                        .HasRequired(a => a.FullArticle)
                        .WithRequiredPrincipal();

            modelBuilder.Entity<Article>().ToTable("Articles");
            modelBuilder.Entity<FullArticle>().ToTable("Articles");
        }
    }
}
