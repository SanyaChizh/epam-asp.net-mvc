﻿using Blog.Domain.Core.AbstractModels;
using System;
using System.Data.Entity;
using System.Linq;

namespace Blog.Infrastructure.Data.EntityFramework.Repository
{
    public abstract class BasicRepository<T> where T : Entity
    {
        protected BasicRepository(ApplicationContext dbContext, DbSet<T> dbSet)
        {
            _dbContext = dbContext;
            _dbSet = dbSet;
        }

        public IQueryable<T> GetAll()
        {
            return _dbSet;
        }

        public T Get(int id)
        {
            return _dbSet.Find(id);
        }

        public IQueryable<T> Find(Func<T, bool> predicate)
        {
            return _dbSet.Where(predicate).AsQueryable();
        }

        public void Create(T item)
        {
            _dbSet.Add(item);
        }

        public void Update(T item)
        {
            _dbContext.Entry(item).State = EntityState.Modified;
        }

        public void Delete(T item)
        {
            _dbSet.Remove(item);
        }

        public int Count()
        {
            return _dbSet.Count();
        }

        private ApplicationContext _dbContext;
        private DbSet<T> _dbSet;
    }
}
