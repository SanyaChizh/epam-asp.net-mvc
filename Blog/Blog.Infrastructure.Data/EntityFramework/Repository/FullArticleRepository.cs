﻿using Blog.Domain.Core.Models;
using Blog.Domain.Interfaces.Repository;

namespace Blog.Infrastructure.Data.EntityFramework.Repository
{
    class FullArticleRepository : BasicRepository<FullArticle>, IFullArticleRepository
    {
        public FullArticleRepository(ApplicationContext dbContext)
            : base(dbContext, dbContext.FullArticles)
        {
        }
    }
}
