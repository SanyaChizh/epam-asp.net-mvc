﻿using Blog.Domain.Core.Models;
using Blog.Domain.Interfaces.Repository;

namespace Blog.Infrastructure.Data.EntityFramework.Repository
{
    public class ArticleRepository : BasicRepository<Article>, IArticleRepository
    {
        public ArticleRepository(ApplicationContext dbContext)
            : base(dbContext, dbContext.Articles)
        {
        }
    }
}
