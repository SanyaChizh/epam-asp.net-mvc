﻿using Blog.Domain.Core.Models;
using Blog.Domain.Interfaces.Repository;

namespace Blog.Infrastructure.Data.EntityFramework.Repository
{
    public class CommentRepository : BasicRepository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationContext dbContext)
            : base(dbContext, dbContext.Comments)
        {
        }
    }
}
