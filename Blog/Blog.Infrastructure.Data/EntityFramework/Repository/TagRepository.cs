﻿using Blog.Domain.Core.Models;
using Blog.Domain.Interfaces.Repository;

namespace Blog.Infrastructure.Data.EntityFramework.Repository
{
    public class TagRepository : BasicRepository<Tag>, ITagRepository
    {
        public TagRepository(ApplicationContext dbContext)
            : base(dbContext, dbContext.Tags)
        {
        }
    }
}
