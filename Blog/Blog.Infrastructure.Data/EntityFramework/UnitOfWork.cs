﻿using Blog.Domain.Interfaces;
using Blog.Domain.Interfaces.Repository;
using Blog.Infrastructure.Data.EntityFramework.Repository;
using System;

namespace Blog.Infrastructure.Data.EntityFramework
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork()
        {
            _dbContext = new ApplicationContext();
            _articleRepository = new Lazy<ArticleRepository>(() => new ArticleRepository(_dbContext));
            _commentRepository = new Lazy<CommentRepository>(() => new CommentRepository(_dbContext));
            _fullArticleRepository = new Lazy<FullArticleRepository>(() => new FullArticleRepository(_dbContext));
            _tagRepository = new Lazy<TagRepository>(() => new TagRepository(_dbContext));
        }

        public IArticleRepository Articles => _articleRepository.Value;
        public ICommentRepository Comments => _commentRepository.Value;
        public IFullArticleRepository FullArticles => _fullArticleRepository.Value;
        public ITagRepository Tags => _tagRepository.Value;

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _dbContext.Dispose();
            }
            _disposed = true;
        }

        private ApplicationContext _dbContext;
        private bool _disposed = false;
        private Lazy<ArticleRepository> _articleRepository;
        private Lazy<CommentRepository> _commentRepository;
        private Lazy<FullArticleRepository> _fullArticleRepository;
        private Lazy<TagRepository> _tagRepository;
    }
}
