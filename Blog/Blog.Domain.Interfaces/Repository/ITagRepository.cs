﻿using Blog.Domain.Core.Models;

namespace Blog.Domain.Interfaces.Repository
{
    public interface ITagRepository : IRepository<Tag>
    {
    }
}
