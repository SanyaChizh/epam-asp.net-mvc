﻿using Blog.Domain.Core.AbstractModels;
using System;
using System.Linq;

namespace Blog.Domain.Interfaces.Repository
{
    public interface IRepository<T> where T : Entity
    {
        IQueryable<T> GetAll();
        T Get(int id);
        IQueryable<T> Find(Func<T, bool> predicate);
        void Create(T item);
        void Update(T item);
        void Delete(T item);
        int Count();
    }
}
