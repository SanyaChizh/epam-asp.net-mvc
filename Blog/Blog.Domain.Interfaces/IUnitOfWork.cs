﻿using Blog.Domain.Interfaces.Repository;
using System;

namespace Blog.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IArticleRepository Articles { get; }
        ICommentRepository Comments { get; }
        IFullArticleRepository FullArticles { get; }
        ITagRepository Tags { get; }
        void Save();
    }
}
