﻿using System.ComponentModel.DataAnnotations;

namespace Blog.ViewModels.Identity
{
    public class LoginModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}