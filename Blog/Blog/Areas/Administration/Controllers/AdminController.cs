﻿using Blog.Areas.Administration.ViewModels;
using Blog.Domain.Core.Models;
using Blog.Infrastructure.Data.EntityFramework;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Blog.Areas.Administration.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        public AdminController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CreateArticle()
        {
            var model = new ArticleViewModel()
            {
                Tags = new MultiSelectList(_unitOfWork.Tags.GetAll(), "Id", "Text")
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateArticle(ArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var tags = new List<Tag>();

                foreach (var id in model.SelectedTagIds)
                {
                    tags.Add(_unitOfWork.Tags.Get(id));
                }

                var article = new Article()
                {
                    PublicationDate = DateTime.Now,
                    FullArticle = new FullArticle { Text = model.Text },
                    Text = model.Text.Length > 200 ? model.Text.Substring(0, 200) : model.Text,
                    Name = model.Name,
                    Tags = tags
                };

                _unitOfWork.Articles.Create(article);
                _unitOfWork.Save();

                return RedirectToAction("Index", new { Controller = "Admin", Area = "Administration" });
            }

            return View(model);
        }

        private UnitOfWork _unitOfWork;
    }
}