﻿using System.Web.Mvc;

namespace Blog.Areas.Administration.ViewModels
{
    public class ArticleViewModel
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public MultiSelectList Tags { get; set; }
        public int[] SelectedTagIds { get; set; }
    }
}