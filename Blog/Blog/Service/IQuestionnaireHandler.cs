﻿using Blog.ViewModels;
using System.Collections.Generic;

namespace Blog.Service
{
    public interface IQuestionnaireHandler
    {
        IList<string> Handle(Questionnaire answers);
    }
}
