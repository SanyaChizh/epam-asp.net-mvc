﻿using Blog.Business;
using Blog.Domain.Interfaces;
using Blog.Infrastructure.Data.EntityFramework;
using Blog.Service;
using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Blog.Util
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            kernel.Bind<IQuestionnaireHandler>().To<QuestionnaireHandler>();
        }
    }
}