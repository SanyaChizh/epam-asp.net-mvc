﻿using System.ComponentModel.DataAnnotations;

namespace Blog.ViewModels
{
    public class Questionnaire
    {
        [Required]
        public string Events { get; set; }

        [Required]
        public string BirthdayEffect { get; set; }

        [Required]
        public string LikeUniversityOrJob { get; set; }

        [Required]
        public string Hobby { get; set; }

        [Required]
        public int Knowledge { get; set; }

        public bool Friendly { get; set; }
        public bool Perseverance { get; set; }
        public bool Punctuality { get; set; }
    }
}