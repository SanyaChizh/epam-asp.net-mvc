﻿using Blog.Domain.Core.Models;
using System.Collections.Generic;

namespace Blog.ViewModels
{
    public class ArticlesWithTag
    {
        public IList<Article> Articles { get; set; }
        public string Tag { get; set; }

        public ArticlesWithTag()
        {
            Articles = new List<Article>();
        }
    }
}