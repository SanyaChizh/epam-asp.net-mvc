﻿using Blog.Domain.Core.Models;
using Blog.Domain.Core.Models.Identity;
using Blog.Infrastructure.Data.EntityFramework;
using Blog.Infrastructure.Data.EntityFramework.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Blog.EntityFramework
{
    public class BlogDbInitializer : DropCreateDatabaseAlways<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            string firstText = @"Вы заметили, что Фейсбук обрёл сверхъестественную способность распознавать ваших друзей на ваших фотографиях? 
                  В старые времена Фейсбук отмечал ваших друзей на фотографиях лишь после того, 
                  как вы щёлкали соответствующее изображение и вводили через клавиатуру имя вашего друга. 
                  Сейчас после вашей загрузки фотографии Фейсбук отмечает любого для вас, что похоже на волшебство.
                  Фейсбук автоматически маркирует людей на ваших фотографиях, которых вы отметили когда-то ранее. 
                  Я не могу определиться для себя, полезно это или жутковато!
                  Эта технология называется распознавание лиц. Алгоритмы Фейсбука могут распознавать лица ваших друзей после того, 
                  как вы отметили их лишь пару-тройку раз. 
                  Это — удивительная технология: Фейсбук в состоянии распознавать лица с точностью 98% — практически так же, как и человек!
                  Как использовать обучение машины при очень сложной проблеме
                  До сих пор в частях 1, 2 и 3 мы использовали обучение машины для решения изолированных проблем, 
                  имеющих только один шаг — оценка стоимости дома, создание новых данных на базе существующих и определение, содержит ли изображение некоторый объект. 
                  Все эти проблемы могут быть решены выбором одного алгоритма обучения машины, вводом данных и получением результата.
                  Но распознавание лиц представляет собой фактически последовательность нескольких связанных проблем:
                  1. Во-первых, необходимо рассмотреть изображение и найти на нём все лица.
                  2. Во-вторых, необходимо сосредоточиться на каждом лице и определить,
                  что, несмотря на неестественный поворот лица или неважное освещение, это — один и тот же человек.
                  3. В-третьих, надо выделить уникальные характеристики лица,
                  которые можно использовать для отличия его от других людей — например, размер глаз, удлинённость лица и т.п.
                  4. В завершение необходимо сравнить эти уникальные характеристики лица с характеристиками других известных вам людей, 
                  чтобы определить имя человека.
                  Мозг человека проделывает всё это автоматически и мгновенно. 
                  Фактически, люди чрезвычайно хорошо распознают лица и, в конечном итоге, видят лица в повседневных предметах";

            string secondText = @"'A' (or 'an') is used to talk about things which are not specific.
                  These are usually things that haven't been mentioned before or that the listener is unfamiliar with.";

            string thirdText = @"Let's say I tell you: 'I went to see a doctor last week.'
                  Explanation: I went to see some doctor.I didn't mention him before, and you are not familiar with him. 
                  Another option is that it is not important who he is. So I use the word 'a'.";

            Tag learningTag = new Tag { Text = "ОБУЧЕНИЕ" };
            Tag englishTag = new Tag { Text = "АНГЛИЙСКИЙ" };
            Tag russianTag = new Tag { Text = "РУССКИЙ" };

            context.Tags.AddRange(new List<Tag>() { learningTag, englishTag, russianTag });
            context.SaveChanges();

            ICollection<Article> articles = new List<Article>
            {
                new Article
                {
                    PublicationDate = DateTime.Now,
                    FullArticle = new FullArticle { Text = firstText },
                    Text = firstText.Length > 200 ? firstText.Substring(0,200) : firstText,
                    Name = "Обучение машины — забавная штука: современное распознавание лиц с глубинным обучением",
                    Tags = new List<Tag>() { learningTag, russianTag }
                },

                new Article
                {
                    PublicationDate = new DateTime(2015, 5, 14),
                    FullArticle = new FullArticle { Text = secondText },
                    Text = secondText.Length > 200 ? secondText.Substring(0, 200) : secondText,
                    Name = "When to use 'a' or 'an'",
                    Tags = new List<Tag>() { learningTag, englishTag }
                },

                new Article
                {
                    PublicationDate = new DateTime(2016, 3, 25),
                    FullArticle = new FullArticle { Text = thirdText },
                    Text = thirdText.Length > 200 ? thirdText.Substring(0, 200) : thirdText,
                    Name = "Example",
                    Tags = new List<Tag>() { learningTag, englishTag }
                }
            };
            context.Articles.AddRange(articles);
            context.SaveChanges();

            ICollection<Comment> comments = new List<Comment>
            {
                new Comment
                {
                    PublicationDate = new DateTime(2014, 8, 5),
                    Text = "Good author",
                    Author = "Vasya"
                },

                new Comment
                {
                    PublicationDate = new DateTime(2016, 1, 22),
                    Text = "Author is very well!",
                    Author = "Petya"
                },

                new Comment
                {
                    PublicationDate = new DateTime(2015, 11, 3),
                    Text = "Bad author",
                    Author = "Grisha"
                }
            };
            context.Comments.AddRange(comments);

            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var userRole = new ApplicationRoleManager(new RoleStore<ApplicationRole>(context));

            var adminRole = new ApplicationRole() { Name = "Admin" };
            userRole.Create(adminRole);

            var admin = new ApplicationUser()
            {
                Email = "oleksandr.chyzh@nure.ua",
                UserName = "oleksandr.chyzh@nure.ua"
            };

            userManager.Create(admin, "password");
            userManager.AddToRole(admin.Id, "Admin");

            base.Seed(context);
        }
    }
}