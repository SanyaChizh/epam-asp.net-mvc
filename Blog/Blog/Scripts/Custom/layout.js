﻿; (function ($, undefined) {
    var menu = $('.dropdown-menu');

    $('#menuButton').hover(
        function (event) {
            menu.stop(true, true).delay(200).fadeIn();
        },
        function (event) {
            menu.stop(true, true).delay(200).fadeOut();
        });

    menu.hover(
        function (event) {
            menu.stop(true, true).delay(200).fadeIn();
        },
        function (event) {
            menu.stop(true, true).delay(200).fadeOut();
        });

})(jQuery);