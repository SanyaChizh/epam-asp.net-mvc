﻿; (function ($, undefined) {
    var isOpen = false;

    $("#createCommentButton").click(function (event) {
        var commentButton = event.target;

        if (isOpen) {
            commentButton.innerText = "Оставить комментарий";
            $("#commentForm")[0].innerHTML = "";
            isOpen = false;
            return false;
        }
        else {
            commentButton.innerHTML = "<span class='glyphicon glyphicon-eye-close'></span> Скрыть";
            isOpen = true;
        }
    });
}(jQuery));