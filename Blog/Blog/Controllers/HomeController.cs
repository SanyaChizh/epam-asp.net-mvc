﻿using Blog.Domain.Core.Models;
using Blog.Domain.Interfaces;
using Blog.Service;
using Blog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(IUnitOfWork unitOfWork, IQuestionnaireHandler questionnaireHandler)
        {
            _unitOfWork = unitOfWork;
            _questionnaireHandler = questionnaireHandler;
        }

        [HttpGet]
        public ViewResult Index()
        {
            if (Session["wasVoted"] == null)
            {
                Session["wasVoted"] = false;
            }

            List<Article> articles = _unitOfWork.Articles.GetAll().ToList();
            articles.Reverse();
            return View(articles);
        }

        [HttpGet]
        public ViewResult Guest()
        {
            List<Comment> comments = _unitOfWork.Comments.GetAll().ToList();
            comments.Reverse();
            return View(comments);
        }

        [HttpGet]
        public PartialViewResult Comment()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Comment(Comment comment)
        {
            comment.PublicationDate = DateTime.Now;

            _unitOfWork.Comments.Create(comment);
            _unitOfWork.Save();

            return Redirect("/Home/Guest");
        }

        [HttpGet]
        public ViewResult ShowFullArticle(int id)
        {
            Article article = _unitOfWork.Articles.Get(id);
            return View(article);
        }

        [HttpPost]
        public RedirectToRouteResult Voting(Vote vote)
        {
            Session["wasVoted"] = true;
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ViewResult ArticlesWithTag(string tag)
        {
            IList<Article> articles = _unitOfWork.Articles.Find(a => a.Tags.Any(t => t.Text.ToUpper() == tag.ToUpper())).ToList();

            var model = new ArticlesWithTag()
            {
                Articles = articles,
                Tag = tag
            };

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ViewResult Questionnaire()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ViewResult Questionnaire(Questionnaire answers)
        {
            if (ModelState.IsValid)
            {
                IList<string> result = _questionnaireHandler.Handle(answers);

                return View("ShowQuestionnaireResult", result);
            }
            return View(answers);
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }

        private IUnitOfWork _unitOfWork;
        private IQuestionnaireHandler _questionnaireHandler;
    }
}