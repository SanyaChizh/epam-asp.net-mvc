﻿using Blog.Service;
using Blog.ViewModels;
using System.Collections.Generic;

namespace Blog.Business
{
    public class QuestionnaireHandler : IQuestionnaireHandler
    {
        public IList<string> Handle(Questionnaire answers)
        {
            IList<string> result = new List<string>();


            string answer = answers.BirthdayEffect == "yes" ? "Вы верите в гороскопы" : "Вы не верите в гороскопы";
            result.Add(answer);

            if (answers.LikeUniversityOrJob == "yes")
            {
                result.Add("Вы трудолюбивый");
            }

            answer = "Вы оцениваете свои знания как ";

            answer += answers.Knowledge == 1 ? "слабые" :
                      answers.Knowledge == 2 ? "ниже среднего" :
                      answers.Knowledge == 3 ? "средние" :
                      answers.Knowledge == 4 ? "выше среднего" : "сильные";
            result.Add(answer);

            int positiveQualitiesNumber = 0;
            if (answers.Friendly) positiveQualitiesNumber++;
            if (answers.Perseverance) positiveQualitiesNumber++;
            if (answers.Punctuality) positiveQualitiesNumber++;

            answer = positiveQualitiesNumber == 0 ? "Вам будет сложно работать в компании" :
                     positiveQualitiesNumber == 1 ? "Вам будет сложновато работать в компании" :
                     positiveQualitiesNumber == 2 ? "Вам будет достаточно легко работать в компании" : "Вам будет очень легко работать в компании";
            result.Add(answer);

            return result;
        }
    }
}