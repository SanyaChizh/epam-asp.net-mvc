﻿using Blog.Domain.Core.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Blog.Helpers
{
    public static class ArticleListHelper
    {
        public static MvcHtmlString CreateList(this HtmlHelper html, IList<Article> articles)
        {
            string result = "";

            foreach (var article in articles)
            {
                result += GetArticleString(article);
            }

            return new MvcHtmlString(result);
        }

        private static string GetArticleString(Article article)
        {
            var divPanel = new TagBuilder("div");
            divPanel.AddCssClass("panel panel-default");

            var divPanelHeading = new TagBuilder("div");
            divPanelHeading.AddCssClass("panel-heading");

            var spanName = new TagBuilder("span");
            spanName.AddCssClass("name");
            spanName.InnerHtml = "Статья: " + article.Name;

            var spanDate = new TagBuilder("date");
            spanDate.AddCssClass("date");
            spanDate.InnerHtml = "Дата публикации: " + article.PublicationDate;

            divPanelHeading.InnerHtml = spanName.ToString();
            divPanelHeading.InnerHtml += spanDate.ToString();

            var divPanelBody = new TagBuilder("div");
            divPanelBody.AddCssClass("panel-body text");
            divPanelBody.InnerHtml = article.Text;

            var a = new TagBuilder("a");
            a.InnerHtml = "Показать весь текст";
            a.Attributes["href"] = "/Home/ShowFullArticle/" + article.Id;
            divPanelBody.InnerHtml += " " + a.ToString();

            divPanel.InnerHtml = divPanelHeading.ToString();
            divPanel.InnerHtml += divPanelBody.ToString();

            return divPanel.ToString();
        }
    }
}