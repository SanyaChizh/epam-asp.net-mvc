﻿using Blog.Domain.Core.AbstractModels;
using System.Collections.Generic;

namespace Blog.Domain.Core.Models
{
    public class Article : Record
    {
        public string Name { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual FullArticle FullArticle { get; set; }

        public Article()
        {
            Tags = new List<Tag>();
        }
    }
}
