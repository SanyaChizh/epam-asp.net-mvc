﻿using Blog.Domain.Core.AbstractModels;
using System.Collections.Generic;

namespace Blog.Domain.Core.Models
{
    public class Tag : Entity
    {
        public string Text { get; set; }
        public ICollection<Article> Articles { get; set; }

        public Tag()
        {
            Articles = new List<Article>();
        }
    }
}
