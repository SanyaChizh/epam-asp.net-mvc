﻿using Blog.Domain.Core.AbstractModels;

namespace Blog.Domain.Core.Models
{
    public class FullArticle : Entity
    {
        public string Text { get; set; }

        public FullArticle() { }
    }
}
