﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Blog.Domain.Core.Models.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
