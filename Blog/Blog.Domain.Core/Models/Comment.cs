﻿using Blog.Domain.Core.AbstractModels;

namespace Blog.Domain.Core.Models
{
    public class Comment : Record
    {
        public string Author { get; set; }

        public Comment() { }
    }
}
