﻿namespace Blog.Domain.Core.AbstractModels
{
    public abstract class Entity
    {
        public int Id { get; set; }

        public Entity() { }
    }
}
