﻿using System;

namespace Blog.Domain.Core.AbstractModels
{
    public abstract class Record : Entity
    {
        public DateTime PublicationDate { get; set; }
        public string Text { get; set; }

        public Record() { }
    }
}
